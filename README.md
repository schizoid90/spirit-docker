# README #

This is the git repo for my Spirit Forum Docker image. More information about Spirit here spirit-project.com

# How to Install #

docker pull schizoid90/spirit

# How to Run #

docker run -dit -p 80:80 spirit

docker attach <container_name>

python /var/www/html/Spirit/example/manage.py spiritinstall

uwsgi --ini /etc/uwsgi/spirit.ini



Then navigate to your server on port 80

# Get code #

git clone https://bitbucket.org/schizoid90/spirit-docker.git