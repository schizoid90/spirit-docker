FROM ubuntu:16.04
MAINTAINER Schizoid90 "https://hub.docker.com/r/schizoid90"

# install required apt packages
RUN apt-get update
RUN apt-get -y install\
	git\
	libfreetype6-dev\
	libjpeg-dev\
	libjpeg8-dev\
	libpng3\
	mariadb-server\
	nginx\
	python-dev\
	python-pip 

# install required pip packages
COPY files/requirements.txt /usr/local/src/requirements.txt
RUN pip install --upgrade pip
RUN pip install -r /usr/local/src/requirements.txt

# get the latest build
RUN git clone https://github.com/nitely/Spirit.git /var/www/html/Spirit

# set up everything for uwsgi
COPY files/uwsgi_params /var/www/html/Spirit/uwsgi_params
COPY files/spirit.ini /etc/uwsgi/spirit.ini
RUN touch /var/www/html/Spirit/example/django.log\
	&& chmod 666 /var/www/html/Spirit/example/django.log
RUN mkdir /var/log/uwsgi

# set up everything for nginx
COPY files/01spirit.conf /etc/nginx/sites-available/01spirit.conf
RUN ln -s /etc/nginx/sites-available/01spirit.conf /etc/nginx/sites-enabled/01spirit.conf
RUN rm -f /etc/nginx/sites-enabled/default

# set our environment variables
RUN echo "export PYTHONPATH=${PYTHONPATH}:/var/www/html/Spirit:/var/www/html/Spirit/example" >> ~/.bashrc
RUN echo "export DJANGO_SETTINGS_MODULE=example.project.settings.dev" >> ~/.bashrc
RUN echo "export SECRET_KEY=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-50};echo;`" >> ~/.bashrc
RUN sed -i 's/SECRET_KEY = "DEV"/SECRET_KEY = os.environ.get("SECRET_KEY", "")/g' /var/www/html/Spirit/example/project/settings/dev.py

# run Djagno migrations
#RUN python /var/www/html/Spirit/example/manage.py migrate

# get everything ready for testing
EXPOSE 80
CMD service nginx start && /bin/bash
